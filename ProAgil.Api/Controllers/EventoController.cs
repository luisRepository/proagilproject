﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProAgil.Api.Dto;
using ProAgil.Domain.Model;
using ProAgil.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProAgil.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventoController : ControllerBase
    {
        private readonly IProAgilRepository repository;
        private readonly IMapper mapper;

        public EventoController(IProAgilRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> ObterTodosEventos()
        {
            try
            {
                var eventos = await repository.ObterTodosEventosAsync(true);

                var result = mapper.Map<EventoDto[]>(eventos);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Erro interno no servidor {ex.Message}");
            }
        }

        [HttpPost("Upload")]
        public async Task<IActionResult> Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName;
                    var fullPath = Path.Combine(pathToSave, fileName.Replace("\"", " ").Trim());

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Erro interno no servidor {ex.Message}");
            }

            return BadRequest("Erro ao tentar realizar upload");
        }

        [HttpGet("{eventoId}")]
        public async Task<IActionResult> ObterEventoPorId(int EventoId)
        {
            try
            {
                var evento = await repository.ObterEventoAsyncPorId(EventoId, true);

                var result = mapper.Map<EventoDto>(evento);

                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro interno no servidor");
            }
        }

        [HttpGet("obterTema/{tema}")]
        public async Task<IActionResult> ObterEventoPorTema(string tema)
        {
            try
            {
                var evento = await repository.ObterTodosEventosAsyncPorTema(tema, true);

                var result = mapper.Map<EventoDto[]>(evento);

                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro interno no servidor");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AdicionarEvento([FromBody] EventoDto eventoDto)
        {
            try
            {
                var evento = mapper.Map<Evento>(eventoDto);

                repository.Add(evento);

                if (await repository.SaveChangesAsync())
                {
                    return Created($"/api/evento/{evento.EventoId}", mapper.Map<EventoDto>(eventoDto));
                }

                return BadRequest();

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro interno no servidor");
            }
        }

        [HttpPut("{eventoId}")]
        public async Task<IActionResult> AtualizarEvento(int eventoId, [FromBody] EventoDto eventoDto)
        {
            try
            {
                var evento = await repository.ObterEventoAsyncPorId(eventoId, false);
                if (evento == null) return NotFound();

                var idLotes = new List<int>();
                var idRedesSociais = new List<int>();

                eventoDto.Lotes.ForEach(item => idLotes.Add(item.Id));
                eventoDto.RedesSociais.ForEach(item => idRedesSociais.Add(item.Id));

                var lotes = evento.Lotes.Where(
                    lote => !idLotes.Contains(lote.Id)).
                    ToArray();

                var redesSociais = evento.RedesSociais.Where(
                    rede => !idRedesSociais.Contains(rede.Id))
                    .ToArray();

                if (lotes.Length > 0) repository.DeleteRange(lotes);
                if (redesSociais.Length > 0) repository.DeleteRange(redesSociais);

                mapper.Map(eventoDto, evento);

                repository.Update(evento);

                if (await repository.SaveChangesAsync())
                {
                    return Created($"/api/evento/{evento.EventoId}", evento);
                }

                return BadRequest();

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro interno no servidor");
            }
        }

        [HttpDelete("{eventoId}")]
        public async Task<IActionResult> ExcluirEvento(int eventoId)
        {
            try
            {
                var response = await repository.ObterEventoAsyncPorId(eventoId, false);

                if (response == null) return NotFound();

                repository.Delete(response);

                if (await repository.SaveChangesAsync())
                {
                    return Ok();
                }

                return BadRequest();

            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro interno no servidor");
            }
        }
    }
}