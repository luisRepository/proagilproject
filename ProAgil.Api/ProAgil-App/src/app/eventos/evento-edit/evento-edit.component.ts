import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { EventoService } from 'src/app/services/evento.service';
import { Evento } from 'src/app/models/Evento';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-evento-edit',
  templateUrl: './evento-edit.component.html',
  styleUrls: ['./evento-edit.component.scss']
})
export class EventoEditComponent implements OnInit {

  titulo = 'Editar Evento';
  registerForm: FormGroup;
  evento: Evento = new Evento();
  imagemUrl = '';
  fileNameToUpdate: string;
  dataAtual = '';
  file: File;

  get lotes(): FormArray {
    return <FormArray>this.registerForm.get('lotes');
  }

  get redesSociais(): FormArray {
    return <FormArray>this.registerForm.get('redesSociais');
  }

  constructor(
    private eventoService: EventoService,
    private router: ActivatedRoute,
    private fb: FormBuilder,
    private localeService: BsLocaleService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.validation();
    this.carregarEvento();
  }

  carregarEvento() {
    const idEvento = +this.router.snapshot.paramMap.get('id');
    this.eventoService.obterEventosPorId(idEvento).subscribe(
      (evento: Evento) => {
        this.evento = Object.assign({}, evento);
        this.fileNameToUpdate = evento.imagemUrl.toString();

        this.imagemUrl = `http://localhost:5000/resources/images/${this.evento.imagemUrl}?_ts=${this.dataAtual}`;
        this.evento.imagemUrl = '';
        this.registerForm.patchValue(this.evento);

        this.evento.lotes.forEach(lote => {
          this.lotes.push(this.criarLote(lote));
        });
        this.evento.redesSociais.forEach(redeSocial => {
          this.redesSociais.push(this.criarRedeSocial(redeSocial));
        });
      }
    );
  }

  validation() {
    this.registerForm = this.fb.group({
      idEvento: [],
      tema: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      local: ['', [Validators.required]],
      dataEvento: ['', [Validators.required]],
      qtdPessoas: ['', [Validators.required, Validators.max(1200000)]],
      imagemUrl: [''],
      telefone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      lotes: this.fb.array([]),
      redesSociais: this.fb.array([])
    });
  }

  criarLote(lote: any): FormGroup {
    return this.fb.group({
      id: [lote.id],
      nome: [lote.nome, [Validators.required]],
      quantidade: [lote.quantidade, [Validators.required]],
      preco: [lote.preco, [Validators.required]],
      dataInicio: [lote.dataInicio],
      dataFim: [lote.dataFim]
    });
  }

  criarRedeSocial(redeSocial: any): FormGroup {
    return this.fb.group({
      id: [redeSocial.id],
      nome: [redeSocial.nome, [Validators.required]],
      url: [redeSocial.url, [Validators.required]]
    });
  }

  onFileChange(evento: any, file: FileList) {
    const reader = new FileReader();
    reader.onload = (event: any) => this.imagemUrl = event.target.result;
    this.file = evento.target.files;
    reader.readAsDataURL(file[0]);
  }

  adicionarLote() {
    this.lotes.push(this.criarLote({ id: 0 }));
  }

  adicionarRedesSociais() {
    this.redesSociais.push(this.criarRedeSocial({ id: 0 }));
  }

  removerLote(id: number) {
    this.lotes.removeAt(id);
  }

  removerRedeSocial(id: number) {
    this.redesSociais.removeAt(id);
  }

  salvarEvento() {
    this.evento = Object.assign({ eventoId: this.evento.eventoId }, this.registerForm.value);
    this.evento.imagemUrl = this.fileNameToUpdate;
    this.uploadImagem();
    console.log(this.evento)

    this.eventoService.alterarEvento(this.evento).subscribe(() => {
      this.toastr.success('Editado com Sucesso');
    }, error => {
      this.toastr.error('Erro ao tentar alterar');
      console.log(error);
    });
  }

  uploadImagem() {
    if (this.registerForm.get('imagemUrl').value !== '') {
      this.eventoService.incluirUpload(this.file, this.fileNameToUpdate).subscribe(() => {
        this.dataAtual = new Date().getMilliseconds().toString();
        this.imagemUrl = `http://localhost:5000/resources/images/${this.evento.imagemUrl}?_ts=${this.dataAtual}`;
      });
    }
  }

}
