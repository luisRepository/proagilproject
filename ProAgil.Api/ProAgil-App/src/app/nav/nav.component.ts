import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  nome: string;
  jwtHelper = new JwtHelperService();
  constructor(private authService: AuthService, public router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  loggedIn() {
    if (this.authService.loggedIn()) {
      const decodedToken = this.jwtHelper.decodeToken(localStorage.getItem('token'));
      this.nome = decodedToken.unique_name;
    }
    return this.authService.loggedIn();
  }

  logar() {
    this.router.navigate(['/user/login']);
  }

  logout() {
    localStorage.removeItem('token');
    this.toastr.show('Log Out');
    this.router.navigate(['/user/login']);
  }

}
