import { RedeSocial } from './RedeSocial';
import { Lote } from './Lote';

export class Evento {

     constructor() { }

     eventoId: number;
     local: string;
     tema: string;
     dataEvento: Date;
     template: string;
     qtdPessoas: number;
     imagemUrl: string;
     telefone: string;
     email: string;
     lotes: Lote[];
     redesSociais: RedeSocial[];
}
