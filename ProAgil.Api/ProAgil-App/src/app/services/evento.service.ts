import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Evento } from '../models/Evento';

@Injectable({
  providedIn: 'root'
})
export class EventoService {
  baseUrl = 'http://localhost:5000/api/evento';

  constructor(private http: HttpClient) {}

  obterEventos(): Observable<Evento[]> {
    return this.http.get<Evento[]>(`${this.baseUrl}`);
  }

  obterEventosPorId(id: number): Observable<Evento> {
    return this.http.get<Evento>(`${this.baseUrl}/${id}`);
  }

  obterEventosPorTema(tema: string): Observable<Evento> {
    return this.http.get<Evento>(`${this.baseUrl}/obterTema/${tema}`);
  }

  incluirEvento(evento: Evento) {
    return this.http.post<Evento>(this.baseUrl, evento);
  }

  alterarEvento(evento: Evento) {
    return this.http.put<Evento>(`${this.baseUrl}/${evento.eventoId}`, evento);
  }

  excluirEvento(eventoId: number) {
    return this.http.delete<Evento>(`${this.baseUrl}/${eventoId}`);
  }

  incluirUpload(file: File, name: string) {
    const fileToUpload = <File>file[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, name);

    return this.http.post<Evento>(`${this.baseUrl}/upload/`, formData);
  }

}
