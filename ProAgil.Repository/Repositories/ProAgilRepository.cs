﻿using Microsoft.EntityFrameworkCore;
using ProAgil.Domain.Model;
using ProAgil.Repository.Data;
using ProAgil.Repository.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProAgil.Repository.Repositories
{
    public class ProAgilRepository : IProAgilRepository
    {

        private readonly ProAgilContext contexto;

        public ProAgilRepository(ProAgilContext contexto)
        {
            this.contexto = contexto;

        }

        public void Add<T>(T entity) where T : class
        {
            contexto.Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            contexto.Update(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            contexto.Remove(entity);
        }

        public void DeleteRange<T>(T[] entityArray) where T : class
        {
            contexto.RemoveRange(entityArray);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await contexto.SaveChangesAsync() > 0;
        }

        //Evento

        public async Task<Evento[]> ObterTodosEventosAsync(bool includePalestrantes = false)
        {
            IQueryable<Evento> query = contexto.Eventos
                .Include(c => c.Lotes)
                .Include(c => c.RedesSociais);

            if (includePalestrantes)
            {
                query = query
                    .Include(pe => pe.PalestranteEventos)
                    .ThenInclude(p => p.Palestrante);
            }

            query = query.AsNoTracking()
                .OrderBy(c => c.EventoId);

            return await query.ToArrayAsync();
        }

        public async Task<Evento> ObterEventoAsyncPorId(int eventoId, bool includePalestrantes)
        {
            IQueryable<Evento> query = contexto.Eventos
                .Include(c => c.Lotes)
                .Include(c => c.RedesSociais);

            if (includePalestrantes)
            {
                query = query
                    .Include(pe => pe.PalestranteEventos)
                    .ThenInclude(p => p.Palestrante);
            }

            query = query
                .AsNoTracking()
                .OrderByDescending(c => c.DataEvento)
                .Where(c => c.EventoId == eventoId);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<Evento[]> ObterTodosEventosAsyncPorTema(string tema, bool includePalestrantes)
        {
            IQueryable<Evento> query = contexto.Eventos
                .Include(c => c.Lotes)
                .Include(c => c.RedesSociais);

            if (includePalestrantes)
            {
                query = query
                    .AsNoTracking()
                    .Include(pe => pe.PalestranteEventos)
                    .ThenInclude(p => p.Palestrante);
            }

            query = query.OrderByDescending(c => c.DataEvento)
                .Where(c => c.Tema.ToLower().Contains(tema.ToLower()));

            return await query.ToArrayAsync();
        }

        //Palestrante 

        public async Task<Palestrante> ObterPalestranteAsync(int palestranteId, bool includeEventos = false)
        {
            IQueryable<Palestrante> query = contexto.Palestrantes
                .Include(c => c.RedesSociais);

            if (includeEventos)
            {
                query = query
                    .Include(pe => pe.PalestranteEventos)
                    .ThenInclude(e => e.Evento);
            }

            query = query.OrderBy(p => p.Nome)
                .Where(p => p.Id == palestranteId);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<Palestrante[]> ObterTodosPalestrantesAsyncPorNome(string nome, bool includeEventos)
        {
            IQueryable<Palestrante> query = contexto.Palestrantes
                .Include(c => c.RedesSociais);

            if (includeEventos)
            {
                query = query
                    .Include(pe => pe.PalestranteEventos)
                    .ThenInclude(e => e.Evento);
            }

            query = query
                .AsNoTracking()
                .Where(p => p.Nome.ToLower() == nome.ToLower());

            return await query.ToArrayAsync();
        }
    }
}
