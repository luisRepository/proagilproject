﻿using ProAgil.Domain.Model;
using System.Threading.Tasks;

namespace ProAgil.Repository.Interfaces
{
    public interface IProAgilRepository
    {
        void Add<T>(T entity) where T : class;

        void Update<T>(T entity) where T : class;

        void Delete<T>(T entity) where T : class;

        void DeleteRange<T>(T[] entity) where T : class;

        Task<bool> SaveChangesAsync();

        //Eventos

        Task<Evento[]> ObterTodosEventosAsyncPorTema(string tema, bool includePalestrantes);

        Task<Evento[]> ObterTodosEventosAsync(bool includePalestrantes);

        Task<Evento> ObterEventoAsyncPorId(int eventoId, bool includePalestrantes);

        //Palestrante

        Task<Palestrante> ObterPalestranteAsync(int palestranteId, bool includeEventos);

        Task<Palestrante[]> ObterTodosPalestrantesAsyncPorNome(string nome, bool includeEventos);

    }
}